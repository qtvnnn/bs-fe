import GetUser from '~/api/getUser'
import UserManager from '~/api/userManager'
import SetUpHardware from '~/api/setUpHardware'
import BookingManage from '~/api/bookingManage'
import Room from '~/api/room'
import AdminCyber from '~/api/adminCyber'
import CyberInfor from '~/api/cyberInfor'
import Slots from '~/api/slots'
import Search from '~/api/search'
import AdminUser from '~/api/adminUser'
import CyberRateUser from '~/api/cyberRateUser'
import UserRateCyber from '~/api/userRateCyber'

export default (context, inject) => {
  // Initialize API factories
  const factories = {
    getUser: GetUser(context.$axios),
    userManager: UserManager(context.$axios),
    setUpHardware: SetUpHardware(context.$axios),
    bookingManage: BookingManage(context.$axios),
    room: Room(context.$axios),
    adminCyber: AdminCyber(context.$axios),
    cyberInfor: CyberInfor(context.$axios),
    slots: Slots(context.$axios),
    search: Search(context.$axios),
    adminUser: AdminUser(context.$axios),
    cyberRateUser: CyberRateUser(context.$axios),
    userRateCyber: UserRateCyber(context.$axios)
  }

  // Inject $api
  inject('api', factories)
  context.$api = factories
}
