// using when get detail from Hash Ipfs. Url similar: data.preview
export default (context, inject) => {
  const getIpfs = (hash, type = 0, callback = null) => {
    if (typeof callback === 'function') {
      callback(hash)
    }
    return 'https://defiforyou.mypinata.cloud/ipfs/' + hash
    // let tryCallWithHost
    // try {
    //   let tryCallWithHost = 0
    //   const { data } = await context.$axios.get(HOST_NAME_GET_IPFS[tryCallWithHost] + hash)

    //   return data
    // } catch (error) {
    //   tryCallWithHost = 1
    //   const { data } = await context.$axios.get(HOST_NAME_GET_IPFS[tryCallWithHost] + hash)

    //   return data
    // }
  }

  // Inject $shortAddress(item) in Vue, context, and store
  inject('getIpfs', getIpfs)
  context.$getIpfs = getIpfs
}
