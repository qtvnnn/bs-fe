import Vue from 'vue'
import Lazyload from 'vue-lazyload'

const loadingImage = require('~/assets/img/rolling.svg')

Vue.use(Lazyload, {
  preLoad: 1.3,
  loading: loadingImage,
  error: 'https://media.giphy.com/media/Ke8JKfxe83FpLrra71/giphy.gif',
  attempt: 1,
  listenEvents: ['scroll']
})
