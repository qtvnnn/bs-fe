export const state = () => ({
  user: {}
})

export const getters = {
  user (state) {
    return state.user
  }
}

export const actions = {
  async changeUser ({ commit }, payload) {
    const { data } = await this.$api.getUser.getProfile(payload)
    commit('CHANGE_USER', data.data)
  }
}

export const mutations = {
  CHANGE_USER (state, data) {
    localStorage.setItem('profileUser', JSON.stringify(data))
    state.user = data
  }
}
