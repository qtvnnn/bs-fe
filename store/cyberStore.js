export const state = () => ({
  listCybers: [],
  listUsers: []
})

export const getters = {}

export const actions = {
  async getCyberList ({ commit }, payload) {
    try {
      const { data } = await this.$api.search.searchCyberByName(payload.pageIndex, payload.pageSize, payload.searchContent)
      commit('SET_CYBER_LIST', data)
    } catch (err) {
      return err
    }
  },
  async getUserList ({ commit }, payload) {
    try {
      const { data } = await this.$api.adminUser.getListUser(payload.pageIndex, payload.pageSize, payload.username)
      commit('SET_USER_LIST', data.data)
    } catch (err) {
      return err
    }
  }
}
export const mutations = {
  SET_CYBER_LIST (currState, data) {
    currState.listCybers = data
  },
  SET_USER_LIST (currState, data) {
    currState.listUsers = data
  }
}
