export default axios => ({
    getRateCyberById (id) {
        return axios.get(`/UserRateCyber/GetRateCyberById?id=${id}`)
    },

    addUserRateCyber (payload) {
        return axios.post(`/UserRateCyber/AddUserRateCyber`, payload)
    },

    updateRateCyber (payload) {
        return axios.post(`/UserRateCyber/UpdateRateCyber`, payload)
    },

    removeRateById (id) {
        return axios.delete(`/UserRateCyber/RemoveRateById?id=${id}`)
    },
})