export default axios => ({
    getListCyberAccount (pageIndex, pageSize) {
        return axios.get(`/User/ViewListCyberAccount?pageIndex=${pageIndex}&pageSize=${pageSize}`)
    },

    getDetailCyberAccount (id) {
        return axios.get(`/User/GetDetailCyberAccount?cyberAccountId=${id}`)
    },

    addNewCyberAccount (payload) {
        return axios.post('/User/AddNewCyberAccount', payload)
    },

    editCyberAccount (payload) {
        return axios.post('/User/EditCyberAccount', payload)
    },

    editUserInfor (payload) {
        return axios.patch('/User/EditUserInfor', payload)
    },

    removeCyberAccount (payload) {
        return axios.delete('/User/RemoveCyberAccount', payload)
    },
    getMyInfor () {
        return axios.get('/User/GetMyInfor')
    },
    changePassword (oldPass, newPass) {
        return axios.post(`/Authentication/ChangePassword?oldPass=${oldPass}&newPass=${newPass}`)
    },
    sendCodeResetPassword (email) {
        return axios.post(`/Authentication/SendCodeResetPassword?email=${email}`)
    },
    confirmPass (email, code) {
        return axios.post(`/Authentication/ConfirmPass?email=${email}&code=${code}`)
    },
    setPassword (accountId, pass) {
        return axios.post(`/Authentication/SetPassword?accountId=${accountId}&pass=${pass}`)
    },
})