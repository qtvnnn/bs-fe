export default axios => ({
    getListCyber () {
        return axios.get('/AdminCyber/ListCyber')
    },

    getDetailCyber (id) {
        return axios.get(`/AdminCyber/ViewDetailCyber?id=${id}`)
    },

    registerCyber (payload) {
        return axios.post(`/AdminCyber/RegisterCyber`, payload)
    },

    verifyCyber (id) {
        return axios.post(`/AdminCyber/VerifyCyber?cyberId=${id}`)
    },

    rejectCyber (id) {
        return axios.delete(`/AdminCyber/RejectCyber?cyberId=${id}`)
    },

    lockCyber (id) {
        return axios.post(`/AdminCyber/LockCyber?cyberId=${id}`)
    },

    unLockCyber (id) {
        return axios.post(`/AdminCyber/UnLockCyber?cyberId=${id}`)
    },

    getListCyberByStatusId (pageIndex, pageSize, statusCyberId, cyberName) {
        return axios.get(`/AdminCyber/ViewListCyberByFilter?pageIndex=${pageIndex}&pageSize=${pageSize}&statusCyberId=${statusCyberId || ''}&cyberName=${cyberName || ''}`)
    },
})