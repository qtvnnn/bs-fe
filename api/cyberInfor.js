export default axios => ({
    editCyberInfor (payload) {
        return axios.patch('/CyberInfor/EditCyberInfor', payload)
    },
    getMyCyber () {
        return axios.get('/CyberInfor/GetMyCyber')
    },
})