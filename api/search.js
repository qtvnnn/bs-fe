export default axios => ({
    getAllCybers () {
        return axios.get('/Search/GetAllCybers?pageIndex=0&pageSize=999')
    },

    searchCyberByName (index, size, name = '') {
        return axios.get(`/Search/SearchCyberByName?pageIndex=${index}&pageSize=${size}&searchContent=${name || ''}`)
    },

    getCyberById (id) {
        return axios.get(`/Search/GetCyberById?id=${id}`)
    },

    getCyberByRate () {
        return axios.get('/Search/ViewCyberByRate')
    },
})