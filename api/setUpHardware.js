export default axios => ({
    getHardwaresByCyberId (id) {
        return axios.get(`/SetUpHardware/GetHardwaresByCyberId?cyberId=${id}`)
    },

    getHardwaresById (id) {
        return axios.get(`/SetUpHardware/GetHardwaresById?hardwareId=${id}`)
    },

    createSlotHardwareConfig (payload) {
        return axios.post('/SetUpHardware/CreateSlotHardwareConfig', payload)
    },

    updateSlotHardwareConfig (payload) {
        return axios.patch(`/SetUpHardware/UpdateSlotHardwareConfig`, payload)
    },

    removeSlotHardwareConfig (id) {
        return axios.delete(`/SetUpHardware/RemoveSlotHardwareConfig?hardwareId=${id}`)
    },
})