export default axios => ({
    getRoomById (id) {
        return axios.get(`/Room/GetRoomById?roomId=${id}`)
    },

    getListRoomByCyberId (pageIndex, pageSize, cyberId) {
        return axios.get(`/Room/GetListRoomByCyberId?pageIndex=${pageIndex}&pageSize=${pageSize}&cyberId=${cyberId}`)
    },

    createNewRoom (payload) {
        return axios.post('/Room/CreateNewRoom', payload)
    },

    editRoomInfor (payload) {
        return axios.patch('/Room/EditRoomInfor', payload)
    },

    editRoomAllow (roomId, maxX, maxY) {
        return axios.patch(`/Room/EditSizeRoom-Allow?roomId=${roomId}&maxX=${maxX}&maxY=${maxY}`)
    },

    editSizeRoom (roomId, maxX, maxY) {
        return axios.patch(`/Room/EditSizeRoom?roomId=${roomId}&maxX=${maxX}&maxY=${maxY}`)
    },

    removeRoom (roomId) {
        return axios.delete(`/Room/RemoveRoom?roomId=${roomId}`)
    },
})