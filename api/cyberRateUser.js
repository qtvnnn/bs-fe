export default axios => ({
    getRateById (id) {
        return axios.get(`/CyberRateUser/GetRateById?id=${id}`)
    },

    addNewRateUser (payload) {
        return axios.post(`/CyberRateUser/AddNewRateUser`, payload)
    },

    updateRateUser (payload) {
        return axios.post(`/CyberRateUser/UpdateRateUser`, payload)
    },

    removeRateById (id) {
        return axios.delete(`/CyberRateUser/RemoveRateById?id=${id}`)
    },
})