export default axios => ({
    getListBookingByCyberId (pageIndex, pageSize, cyberId) {
        return axios.get(`/ManageBooking/GetListBookingByCyberId?pageIndex=${pageIndex}&pageSize=${pageSize}&cyberId=${cyberId}`)
    },

    getListBookingByUserId (pageIndex, pageSize) {
        return axios.get(`/ManageBooking/GetMyListBooking?pageIndex=${pageIndex}&pageSize=${pageSize}`)
    },

    getBookingById (id) {
        return axios.get(`/ManageBooking/GetBookingById?id=${id}`)
    },

    checkSplit (id) {
        return axios.get(`/ManageBooking/CheckSplit?id=${id}`)
    },

    createNewOrder (payload) {
        return axios.post('/ManageBooking/CreateNewOrder', payload)
    },

    changeStatusOrder (orderId, statusOrder) {
        return axios.patch(`/ManageBooking/ChangeStatusOrder?orderId=${orderId}&statusOrder=${statusOrder}`)
    },

    approveOrder (id) {
        return axios.put(`/ManageBooking/ApproveOrder?orderId=${id}`)
    },

    rejectOrder (id) {
        return axios.put(`/ManageBooking/RejectOrder?orderId=${id}`)
    },

    cancelOrder (id) {
        return axios.put(`/ManageBooking/CancelOrder?orderId=${id}`)
    },

    removeCyberAccount (payload) {
        return axios.delete('/User/RemoveCyberAccount', payload)
    },

    filterOrderManager (cyberId, pageIndex, pageSize, orderStatus = 0, roomId = 0) {
        return axios.get(`/ManageBooking/FilterOrderManager?cyberId=${cyberId}&pageIndex=${pageIndex}&pageSize=${pageSize}&orderStatus=${orderStatus || 0}&roomId=${roomId || 0}`)
    },

    getAllCyberBookedByUserId () {
        return axios.get(`/ManageBooking/GetAllCyberBookedByUserId`)
    },
})