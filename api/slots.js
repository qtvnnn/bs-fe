export default axios => ({
    getSlotById (id) {
        return axios.get(`/Slots/GetSlotById?id=${id}`)
    },

    getListSlotByRoom (id) {
        return axios.get(`/Slots/GetListSlotByRoom?roomId=${id}&pageIndex=0&pageSize=999`)
    },

    getListSlotByStatusSlot (roomId, statusSlot) {
        return axios.get(`/Slots/GetListSlotByStatusSlot?roomId=${roomId}&statusSlot=${statusSlot}`)
    },

    createNewSlot (payload) {
        return axios.post('/Slots/CreateNewSlot', payload)
    },

    editSlot (payload) {
        return axios.post(`/Slots/EditSlot`, payload)
    },

    removeSlot (id) {
        return axios.delete(`/Slots/RemoveSlot?slotId=${id}`)
    },

    editStatusSlot (slotId, statusSlot) {
        return axios.patch(`/Slots/EditStatusSlot?slotId=${slotId}&statusSlot=${statusSlot}`)
    },
})