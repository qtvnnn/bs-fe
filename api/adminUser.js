export default axios => ({
    getListUser (pageIndex, pageSize, username) {
        return axios.get(`/AdminUser/SearchUserByName?pageIndex=${pageIndex}&pageSize=${pageSize}&username=${username || ''}`)
    },
    lockUserById (id) {
        return axios.patch(`/AdminUser/LockUserById?userId=${id}`)
    },
    activeUserById (id) {
        return axios.patch(`/AdminUser/UnLockUserById?userId=${id}`)
    }
})