export const USER_ROLE = {
    ADMIN: 1,
    CYBER: 2,
    USER: 3,
}

export const ORDER_STATUS = {
    APPROVE: 1,
    PENDING: 2,
    REJECT: 3,
    CANCEL: 4
}

export const SLOT_STATUS = {
    BUSY: 1,
    FREE: 2,
    MAINTENANCE: 3,
    BOOKED: 4,
    UNLOAD: 5
}

export const CYBER_STATUS = {
    PENDING: 1,
    ACTIVE: 2,
    DEACTIVATE: 3,
    REJECT: 4
}

export const USER_STATUS = {
    ACTIVE: 1,
    BLOCK: 2,
}