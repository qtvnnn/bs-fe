import { USER_ROLE } from '~/enums/roleUser'

export default function ({ store, redirect }) {
  const roleAcound = store.state.auth.user
  // If the user is not authenticated
  if (roleAcound) {
    switch (roleAcound.roleId) {
      case USER_ROLE.ADMIN:
        return redirect('/admin/cyber-manage?pageIndex=1&pageSize=10&statusCyberId=&cyberName=')
      case USER_ROLE.CYBER:
        return redirect('/admin-cyber/information')
    }
  }
}
