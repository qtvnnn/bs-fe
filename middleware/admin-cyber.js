import { USER_ROLE } from '~/enums/roleUser'

export default function ({ store, redirect }) {
  const roleAcound = store.state.auth.user
  // If the user is not authenticated
  if (!store.state.auth.loggedIn) {
    return redirect('/login')
  }
  if (roleAcound) {
    switch (roleAcound.roleId) {
      case USER_ROLE.USER:
        return redirect('/')
      case USER_ROLE.ADMIN:
        return redirect('/admin/cyber-manage?pageIndex=1&pageSize=10&statusCyberId=&cyberName=')
    }
  }
}
